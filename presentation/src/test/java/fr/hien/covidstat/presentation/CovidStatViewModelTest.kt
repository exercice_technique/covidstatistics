package fr.hien.covidstat.presentation

import fr.hien.covidstat.domain.model.CountriesStat
import fr.hien.covidstat.domain.usecase.GetCountriesStatUseCase
import fr.hien.covidstat.presentation.ui.mapper.CovidStatModelMapper
import fr.hien.covidstat.presentation.ui.model.CountryStatUIModel
import fr.hien.covidstat.presentation.ui.model.ErrorUIModel
import fr.hien.covidstat.presentation.ui.model.TextUIModel
import fr.hien.covidstat.presentation.ui.state.CovidStatUIState
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import io.reactivex.Observable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class CovidStatViewModelTest {

    @InjectMockKs
    private lateinit var viewModel: CovidStatViewModel

    @MockK
    private lateinit var getCountriesStatUseCase: GetCountriesStatUseCase

    @MockK
    private lateinit var covidStatModelMapper: CovidStatModelMapper

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @AfterEach
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun initUIState() = runTest {
        // GIVEN
        coEvery { getCountriesStatUseCase() } returns observableCountriesStat
        every { covidStatModelMapper(countriesStat) } returns uiModel

        // WHEN
        viewModel.initUIState()

        // THEN
        coVerify { getCountriesStatUseCase() }
        verify { covidStatModelMapper(countriesStat) }
        val expected = CovidStatUIState.Success(uiModel = uiModel)
        assertEquals(expected, viewModel.uiState.value)
    }

    @Test
    fun `initUIState when error`() = runTest {
        // GIVEN
        coEvery { getCountriesStatUseCase() } returns errorObservableCountriesStat

        // WHEN
        viewModel.initUIState()

        // THEN
        coVerify { getCountriesStatUseCase() }
        assertEquals(errorUIState, viewModel.uiState.value)
    }

    companion object {
        private val countriesStat = CountriesStat(response = emptyList())
        private val observableCountriesStat = Observable.just(countriesStat)
        private val errorObservableCountriesStat = Observable.error<CountriesStat>(Exception())
        private val uiModel: List<CountryStatUIModel> = emptyList()
        private val errorUIState = CovidStatUIState.Error(
            uiModel = ErrorUIModel(
                text = TextUIModel.StringResource(R.string.error_of_get_statistics)
            )
        )
    }
}