package fr.hien.covidstat.presentation.ui.mapper

import fr.hien.covidstat.domain.model.CountriesStat
import fr.hien.covidstat.domain.model.CountryStat
import fr.hien.covidstat.domain.model.Stat
import fr.hien.covidstat.presentation.R
import fr.hien.covidstat.presentation.ui.model.CountryStatUIModel
import fr.hien.covidstat.presentation.ui.model.StatRowUIModel
import fr.hien.covidstat.presentation.ui.model.TextUIModel
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
class CovidStatModelMapperTest {

    @InjectMockKs
    private lateinit var mapper: CovidStatModelMapper

    @ParameterizedTest(name = "WHEN CountriesStat: {0} THEN invoke returns: {1}")
    @MethodSource("provideTestData")
    fun invoke(
        countriesStat: CountriesStat,
        expected: List<CountryStatUIModel>
    ) {
        assertEquals(expected, mapper(countriesStat))
    }

    companion object {
        private val countriesStat = CountriesStat(
            listOf(
                CountryStat(
                    country = "France",
                    population = 70_000_000,
                    cases = Stat(1),
                    deaths = Stat(0),
                    tests = Stat(3)
                )
            )
        )

        @JvmStatic
        private fun provideTestData(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    CountriesStat(response = emptyList()),
                    emptyList<CountryStatUIModel>()
                ),
                Arguments.of(
                    countriesStat,
                    listOf(
                        CountryStatUIModel(
                            name = StatRowUIModel(
                                title = TextUIModel.StringResource(R.string.country),
                                value = TextUIModel.DynamicString("France")
                            ),
                            population = StatRowUIModel(
                                title = TextUIModel.StringResource(R.string.population),
                                value = TextUIModel.DynamicString("70000000")
                            ),
                            cases = StatRowUIModel(
                                title = TextUIModel.StringResource(R.string.cases),
                                value = TextUIModel.DynamicString("1")
                            ),
                            deaths = StatRowUIModel(
                                title = TextUIModel.StringResource(R.string.deaths),
                                value = TextUIModel.DynamicString("0")
                            ),
                            tests = StatRowUIModel(
                                title = TextUIModel.StringResource(R.string.tests),
                                value = TextUIModel.DynamicString("3")
                            )
                        )
                    )
                )
            )
        }
    }
}