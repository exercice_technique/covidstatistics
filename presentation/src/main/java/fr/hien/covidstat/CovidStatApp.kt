package fr.hien.covidstat

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CovidStatApp : Application()