package fr.hien.covidstat.presentation.ui.mapper

import fr.hien.covidstat.domain.model.CountriesStat
import fr.hien.covidstat.presentation.R
import fr.hien.covidstat.presentation.ui.model.CountryStatUIModel
import fr.hien.covidstat.presentation.ui.model.StatRowUIModel
import fr.hien.covidstat.presentation.ui.model.TextUIModel
import javax.inject.Inject

class CovidStatModelMapper @Inject constructor() {

    operator fun invoke(
        countriesStat: CountriesStat
    ): List<CountryStatUIModel> {
        return countriesStat.response.map {
            CountryStatUIModel(
                name = StatRowUIModel(
                    title = TextUIModel.StringResource(R.string.country),
                    value = TextUIModel.DynamicString(it.country)
                ),
                population = StatRowUIModel(
                    title = TextUIModel.StringResource(R.string.population),
                    value = TextUIModel.DynamicString(it.population.toString())
                ),
                cases = StatRowUIModel(
                    title = TextUIModel.StringResource(R.string.cases),
                    value = TextUIModel.DynamicString(it.cases.total.toString())
                ),
                deaths = StatRowUIModel(
                    title = TextUIModel.StringResource(R.string.deaths),
                    value = TextUIModel.DynamicString(it.deaths.total.toString())
                ),
                tests = StatRowUIModel(
                    title = TextUIModel.StringResource(R.string.tests),
                    value = TextUIModel.DynamicString(it.tests.total.toString())
                )
            )
        }
    }
}