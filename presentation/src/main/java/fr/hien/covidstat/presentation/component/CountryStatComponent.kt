package fr.hien.covidstat.presentation.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import fr.hien.covidstat.presentation.ui.model.CountryStatUIModel

@Composable
fun CountryStatComponent(
    modifier: Modifier = Modifier,
    uiModel: CountryStatUIModel
) {
    Card(
        modifier = modifier
    ) {
        Column(
            modifier = modifier.padding(16.dp)
        ) {
            StatRowComponent(uiModel = uiModel.name)
            StatRowComponent(uiModel = uiModel.population)
            StatRowComponent(uiModel = uiModel.cases)
            StatRowComponent(uiModel = uiModel.deaths)
            StatRowComponent(uiModel = uiModel.tests)
        }
    }
}