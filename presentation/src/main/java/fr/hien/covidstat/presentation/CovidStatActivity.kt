package fr.hien.covidstat.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import dagger.hilt.android.AndroidEntryPoint
import fr.hien.covidstat.presentation.component.CountriesStatComponent
import fr.hien.covidstat.presentation.component.ErrorComponent
import fr.hien.covidstat.presentation.component.LoaderComponent
import fr.hien.covidstat.presentation.ui.state.CovidStatUIState
import fr.hien.covidstat.presentation.ui.theme.CovidStatisticsTheme

@AndroidEntryPoint
class CovidStatActivity : ComponentActivity() {

    private val viewModel: CovidStatViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.initUIState()
        setContent {
            CovidStatisticsTheme {
                when (val uiState = viewModel.uiState.collectAsStateWithLifecycle().value) {
                    CovidStatUIState.Loading -> LoaderComponent()
                    is CovidStatUIState.Error -> ErrorComponent(
                        uiModel = uiState.uiModel,
                        onButtonClick = { viewModel.initUIState() }
                    )
                    is CovidStatUIState.Success -> Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        CountriesStatComponent(uiModel = uiState.uiModel)
                    }
                }
            }
        }
    }
}