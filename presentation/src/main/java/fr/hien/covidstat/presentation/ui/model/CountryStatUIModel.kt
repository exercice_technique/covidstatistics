package fr.hien.covidstat.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class CountryStatUIModel(
    val name: StatRowUIModel,
    val population: StatRowUIModel,
    val cases: StatRowUIModel,
    val deaths: StatRowUIModel,
    val tests: StatRowUIModel
)
