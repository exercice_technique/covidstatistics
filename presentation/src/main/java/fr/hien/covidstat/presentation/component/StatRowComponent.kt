package fr.hien.covidstat.presentation.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fr.hien.covidstat.presentation.ui.model.StatRowUIModel

@Composable
fun StatRowComponent(
    modifier: Modifier = Modifier,
    uiModel: StatRowUIModel
) {
    Row(
        modifier = modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        uiModel.title?.let {
            Text(
                text = it.asString(),
                fontWeight = FontWeight.Bold,
                color = Color.Black,
                fontSize = 16.sp
            )
        }
        Spacer(
            modifier = Modifier
                .width(8.dp)
                .weight(1f)
        )
        Text(
            text = uiModel.value.asString(),
            color = Color.Blue,
            fontSize = 16.sp
        )
    }
}