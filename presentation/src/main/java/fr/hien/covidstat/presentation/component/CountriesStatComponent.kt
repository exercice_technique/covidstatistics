package fr.hien.covidstat.presentation.component

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import fr.hien.covidstat.presentation.ui.model.CountryStatUIModel

@Composable
fun CountriesStatComponent(
    modifier: Modifier = Modifier,
    uiModel: List<CountryStatUIModel>
) {
    LazyColumn(
        modifier = modifier.padding(16.dp)
    ) {
        itemsIndexed(uiModel) { index, countryStatUIModel ->
            CountryStatComponent(uiModel = countryStatUIModel)
            if (index != uiModel.lastIndex) {
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }
}