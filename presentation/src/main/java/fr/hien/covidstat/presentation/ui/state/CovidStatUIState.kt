package fr.hien.covidstat.presentation.ui.state

import androidx.compose.runtime.Immutable
import fr.hien.covidstat.presentation.ui.model.CountryStatUIModel
import fr.hien.covidstat.presentation.ui.model.ErrorUIModel

@Immutable
sealed class CovidStatUIState {
    @Immutable
    data object Loading : CovidStatUIState()

    @Immutable
    data class Error(
        val uiModel: ErrorUIModel
    ) : CovidStatUIState()

    @Immutable
    data class Success(
        val uiModel: List<CountryStatUIModel>
    ) : CovidStatUIState()
}