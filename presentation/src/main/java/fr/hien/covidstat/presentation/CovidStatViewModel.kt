package fr.hien.covidstat.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.hien.covidstat.domain.model.CountriesStat
import fr.hien.covidstat.domain.usecase.GetCountriesStatUseCase
import fr.hien.covidstat.presentation.ui.mapper.CovidStatModelMapper
import fr.hien.covidstat.presentation.ui.model.ErrorUIModel
import fr.hien.covidstat.presentation.ui.model.TextUIModel
import fr.hien.covidstat.presentation.ui.state.CovidStatUIState
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CovidStatViewModel @Inject constructor(
    private val getCountriesStatUseCase: GetCountriesStatUseCase,
    private val covidStatModelMapper: CovidStatModelMapper
) : ViewModel() {

    private val _uiState = MutableStateFlow<CovidStatUIState>(CovidStatUIState.Loading)
    val uiState: StateFlow<CovidStatUIState> = _uiState.asStateFlow()

    fun initUIState() {
        viewModelScope.launch {
            getCountriesStatUseCase().subscribeOn(
                Schedulers.io()
            ).subscribe(
                object : Observer<CountriesStat> {

                    override fun onNext(countriesStats: CountriesStat) {
                        _uiState.update {
                            CovidStatUIState.Success(
                                uiModel = covidStatModelMapper(countriesStats)
                            )
                        }
                    }

                    override fun onError(e: Throwable) {
                        _uiState.update {
                            CovidStatUIState.Error(
                                uiModel = ErrorUIModel(
                                    text = TextUIModel.StringResource(R.string.error_of_get_statistics)
                                )
                            )
                        }
                    }

                    override fun onComplete() {}

                    override fun onSubscribe(d: Disposable) {
                        _uiState.update { CovidStatUIState.Loading }
                    }
                }
            )
        }
    }
}