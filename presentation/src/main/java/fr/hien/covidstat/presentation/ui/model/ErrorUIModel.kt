package fr.hien.covidstat.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class ErrorUIModel(
    val text: TextUIModel
)