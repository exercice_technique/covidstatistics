package fr.hien.covidstat.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class StatRowUIModel(
    val title: TextUIModel?,
    val value: TextUIModel
)
