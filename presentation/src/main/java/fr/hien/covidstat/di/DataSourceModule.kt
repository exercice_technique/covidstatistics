package fr.hien.covidstat.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.hien.covidstat.data.source.remote.CountriesStatRemoteDataSourceImpl
import fr.hien.covidstat.domain.source.remote.CountriesStatRemoteDataSource

@InstallIn(SingletonComponent::class)
@Module
abstract class DataSourceModule {

    @Binds
    abstract fun provideCountriesStatRemoteDataSource(
        dataSource: CountriesStatRemoteDataSourceImpl
    ): CountriesStatRemoteDataSource
}
