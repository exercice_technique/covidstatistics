package fr.hien.covidstat.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.hien.covidstat.data.source.remote.CovidWebService
import retrofit2.Retrofit

@InstallIn(SingletonComponent::class)
@Module
class WebServiceModule {

    @Provides
    fun provideLeaguesWS(
        retrofit: Retrofit
    ): CovidWebService = retrofit.create(CovidWebService::class.java)
}
