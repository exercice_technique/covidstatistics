package fr.hien.covidstat.di

import android.annotation.SuppressLint
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.hien.covidstat.presentation.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Provides
    @SuppressLint("HardwareIds")
    @Singleton
    fun provideOkHttp(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
        }
        builder.addInterceptor(
            Interceptor { chain ->
                val request: Request = chain.request()
                val newRequest: Request = request.newBuilder()
                    .addHeader("x-rapidapi-host", "covid-193.p.rapidapi.com")
                    .addHeader("x-rapidapi-key", "ff63e42a9emshd0c9b93c8a3378bp1b89c9jsnf8b65c48fa5e")
                    .build()
                chain.proceed(newRequest)
            }
        )
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideGson(): GsonConverterFactory {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setPrettyPrinting()
        gsonBuilder.registerTypeAdapter(
            LocalDateTime::class.java,
            JsonDeserializer { json, _, _ ->
                LocalDateTime.parse(
                    json.asJsonPrimitive.asString,
                    DateTimeFormatter.ISO_OFFSET_DATE_TIME
                )
            }
        )
        gsonBuilder.registerTypeAdapter(
            OffsetDateTime::class.java,
            JsonDeserializer { json, _, _ ->
                OffsetDateTime.parse(
                    json.asJsonPrimitive.asString,
                    DateTimeFormatter.ISO_OFFSET_DATE_TIME
                )
            }
        )
        gsonBuilder.registerTypeAdapter(
            OffsetDateTime::class.java,
            JsonSerializer { date: OffsetDateTime?, _, _ ->
                val dateString = date?.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                JsonPrimitive(dateString)
            }
        )
        return GsonConverterFactory.create(gsonBuilder.create())
    }
}
