package fr.hien.covidstat.domain.usecase

import fr.hien.covidstat.domain.model.CountriesStat
import fr.hien.covidstat.domain.source.remote.CountriesStatRemoteDataSource
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.reactivex.Observable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class GetCountriesStatUseCaseTest {

    @InjectMockKs
    private lateinit var useCase: GetCountriesStatUseCase

    @MockK
    private lateinit var countriesStatRemoteDataSource: CountriesStatRemoteDataSource

    @Test
    operator fun invoke() = runTest {
        // GIVEN
        val countriesStatObservable: Observable<CountriesStat> = mockk()
        coEvery { countriesStatRemoteDataSource.getCountriesStat() } returns countriesStatObservable

        // WHEN
        val actual = useCase()

        // THEN
        coVerify { countriesStatRemoteDataSource.getCountriesStat() }
        assertEquals(countriesStatObservable, actual)
    }
}