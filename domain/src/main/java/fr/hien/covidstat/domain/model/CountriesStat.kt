package fr.hien.covidstat.domain.model

data class CountriesStat(
    val response: List<CountryStat>
)
