package fr.hien.covidstat.domain.source.remote

import fr.hien.covidstat.domain.model.CountriesStat
import io.reactivex.Observable

interface CountriesStatRemoteDataSource {

    suspend fun getCountriesStat(): Observable<CountriesStat>
}
