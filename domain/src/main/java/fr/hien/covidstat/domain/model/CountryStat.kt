package fr.hien.covidstat.domain.model

data class CountryStat(
    val country: String,
    val population: Int,
    val cases: Stat,
    val deaths: Stat,
    val tests: Stat
)