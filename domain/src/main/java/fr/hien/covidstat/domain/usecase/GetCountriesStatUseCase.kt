package fr.hien.covidstat.domain.usecase

import fr.hien.covidstat.domain.source.remote.CountriesStatRemoteDataSource
import javax.inject.Inject

class GetCountriesStatUseCase @Inject constructor(
    private val countriesStatRemoteDataSource: CountriesStatRemoteDataSource
) {

    suspend operator fun invoke() = countriesStatRemoteDataSource.getCountriesStat()
}