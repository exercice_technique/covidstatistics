package fr.hien.covidstat.data.source.remote

import fr.hien.covidstat.domain.model.CountriesStat
import fr.hien.covidstat.domain.source.remote.CountriesStatRemoteDataSource
import io.reactivex.Observable
import javax.inject.Inject

class CountriesStatRemoteDataSourceImpl @Inject constructor(
    private val covidWebService: CovidWebService
) : CountriesStatRemoteDataSource {
    override suspend fun getCountriesStat(): Observable<CountriesStat> {
        return covidWebService.getCountriesStats()
    }
}