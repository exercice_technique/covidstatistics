package fr.hien.covidstat.data.source.remote

import fr.hien.covidstat.domain.model.CountriesStat
import io.reactivex.Observable
import retrofit2.http.GET

interface CovidWebService {
    @GET("/statistics")
    fun getCountriesStats(): Observable<CountriesStat>
}