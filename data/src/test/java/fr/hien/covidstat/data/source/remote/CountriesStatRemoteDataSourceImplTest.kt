package fr.hien.covidstat.data.source.remote

import fr.hien.covidstat.domain.model.CountriesStat
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class CountriesStatRemoteDataSourceImplTest {

    @InjectMockKs
    private lateinit var dataSourceImpl: CountriesStatRemoteDataSourceImpl

    @MockK
    private lateinit var covidWebService: CovidWebService

    @Test
    fun getCountriesStat() = runTest {
        // GIVEN
        val countriesStatObservable: Observable<CountriesStat> = mockk()
        every { covidWebService.getCountriesStats() } returns countriesStatObservable

        // WHEN
        val actual = dataSourceImpl.getCountriesStat()

        // THEN
        verify { covidWebService.getCountriesStats() }
        assertEquals(countriesStatObservable, actual)
    }
}